-- to be executed from lua-supernn root.
-- $ lua5.1 iris/iris.lua

require('supernn')

supernn.rand_seed()

local net = supernn.Network_make_fcc(4, 3, 3)
net:set_activation(supernn.ACT_SIGMOID_SYMMETRIC)
net:init_weights(-0.5, 0.5)

local t = supernn.NBN()
t:prepare(net)

local train = supernn.Data()
train:load_file("iris/iris.train")
train:calc_bounds()
train:scale(-1, 1)

local e = t:train(net, train, 1e-2, 100)

local printf = function(...) print(string.format(...)) end

printf("Epochs : %d"    , e                           );
printf("MSE    : %f"    , net:calc_mse(train)         );
printf("Class  : %.2f%%", net:calc_class_higher(train));
