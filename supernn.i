%module supernn
%{
#include <supernn>
using namespace SuperNN;
%}

%include "std_string.i"
%include "std_vector.i"

%include "/usr/include/supernn_bits/utils.hpp"
%include "/usr/include/supernn_bits/activation_type.hpp"
%include "/usr/include/supernn_bits/neuron.hpp"

typedef vector<double> Row;
%template(Row) std::vector<double>;

%template(RowVector)    std::vector<SuperNN::Row>;
%template(SInfoVector)  std::vector<SuperNN::SInfo>;
%include "/usr/include/supernn_bits/data.hpp"

%template(NeuronVector) std::vector<SuperNN::Neuron>;
%template(LayerVector)  std::vector<SuperNN::Layer>;
%include "/usr/include/supernn_bits/network.hpp"
%include "/usr/include/supernn_bits/training.hpp"
