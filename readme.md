# About

lua-supernn is a simple binding of SuperNN[1] to Lua 5.1 / luajit. It uses
SWIG[2] to automatically generate the the bindings.

# License

Same as SuperNN.

# Using

    $ make
    $ lua5.1 iris/iris.lua

# Contact

Lucas Hermann Negri - <lucashnegri@gmail.com>

[1]: https://bitbucket.org/lucashnegri/supernn
[2]: http://swig.org/
