CP=cp
MKDIR=mkdir -p
DESTDIR=/
PREFIX=/usr
NAME=supernn.so
CC=g++
PKG_PACKAGES=lua5.1
CXXFLAGS=-O2 -shared -Wall `pkg-config --cflags $(PKG_PACKAGES)` -fPIC
LDFLAGS=-shared `pkg-config --libs $(PKG_PACKAGES)` -lsupernn -o $(NAME)

all: $(NAME)

$(NAME): supernn.i
	swig -c++ -lua -o lua_supernn.cpp supernn.i
	$(CC) $(CXXFLAGS) lua_supernn.cpp $(LDFLAGS)
	
.PHONY: install
install: all
	$(MKDIR) $(DESTDIR)/$(PREFIX)/lib/lua/5.1
	$(CP) $(NAME) $(DESTDIR)/$(PREFIX)/lib/lua/5.1
	
.PHONY: clean
clean:
	rm -f $(NAME) lua_supernn.cpp
